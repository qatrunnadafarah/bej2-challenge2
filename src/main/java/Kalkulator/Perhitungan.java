package Kalkulator;

import java.util.*;

public class Perhitungan {
//    public abstract double hasil();
    static List<Double> dataSekolah;
    static int banyakData;

    public Perhitungan(List<Double> dataSekolah){
        Perhitungan.dataSekolah = dataSekolah;
        Perhitungan.banyakData = dataSekolah.size();
    }
    public Double mean() {
        double total = 0;

        for (int i = 0; i < banyakData; i++) {
            total += dataSekolah.get(i);
        }
        return total / banyakData;
    }

    public Double median() {
        int nilaiTengah = banyakData / 2;

        if (banyakData % 2 != 0){
            return dataSekolah.get(nilaiTengah + 1);
        }else {
            return (dataSekolah.get(nilaiTengah) + dataSekolah.get(nilaiTengah + 1) / 2);
        }
    }

    public List<Double> modus() {
        Map<Double, Integer> dataMap = new HashMap<>();

        for(Double i: dataSekolah) {
            if(dataMap.isEmpty()){
                dataMap.put(i, 1);
            }else {
                if (dataMap.containsKey(i)){
                    dataMap.put(i, dataMap.get(i) + 1);
                }else {
                    dataMap.put(i, 1);
                }
            }
        }

        Integer nilaiTerbesar = Collections.max(dataMap.values());
        List<Double> mod = new ArrayList<>();

        for(Map.Entry<Double, Integer> set: dataMap.entrySet()) {
            if(set.getValue().equals(max_value)) {
                mod.add(set.getKey());
            }
        }
        return mod;
    }

    public static List<Double> newArray(List<List<Double>> dataSekolah) {
        List<Double> Hasil = new ArrayList<>();

        for(List<Double> i: dataSekolah) {
            Hasil.addAll(i);
        }
        return Hasil;
    }
}
