import java.util.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class Main {
    static List<Integer> nilai;
    public static void main(String[] args) {
        System.out.println("----------------");
        System.out.println("Aplikasi Pengolah Nilai Siswa");
        System.out.println("----------------");
        System.out.println("Letakan file csv dengan nama file data_sekolah di direktori berikut: C://temp/direktori");

        Read("D:\\SYNERGY\\Chapter2\\Challenge2\\src\\main\\resources\\data_sekolah.csv");
    }

    public static void fitur() {
        Scanner input = new Scanner(System.in);

        System.out.println();
        System.out.println("Pilih Menu:");
        System.out.println("1. Generate txt untuk menampilkan modus");
        System.out.println("2. Generate txt untuk menampilkan nilai rata-rata, median");
        System.out.println("3. Generate kedua file");
        System.out.println("0. Exit");
        int fitur = input.nextInt();

        if (fitur == 1) {
            TampilModus();
            this.fitur();
        } else if (fitur == 2) {
            TampilMeanMedian();
            this.fitur();
        } else if (fitur == 3) {
//            generateKeduanya();
            this.fitur();
        } else if (fitur == 0) {
            System.exit(0);
        } else {
            System.out.println("!! Input Error. Pilih Lagi !!");
            fitur();
        }

//        challenge();
    }

    public void TampilMeanMedian() {
        Median med = new Median(nilai);
        Mean mean = new Mean(nilai);

        try {
            File file = new File("C://temp/direktori/data_sekolah_mean_median.txt");
            if (file.createNewFile()) {
                System.out.println("New file has been created!");
            }

            FileWriter writer = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(writer);
            bw.write(this.meanMedian(med, mean));
            bw.flush();
            bw.close();
        } catch (IOException var6) {
            var6.printStackTrace();
        }
    }

    private String meanMedian(Median median, Mean mean) {
        StringBuffer data = new StringBuffer("");
        data.append("Median = " + median.getResult()).append("\n").append("Mean = " + mean.getResult());
        return data.toString();
    }

    public static void Read(String fileLocation) {
        try {
            if () {
                File excel = new File(fileLocation);
                FileReader reader = new FileReader(excel);
                BufferedReader br = new BufferedReader(reader);

                String line = "";
                Double[] tempArr;
                List<Double> dataSekolah = new ArrayList<Double>();

                while ((line = br.readLine()) != null) {
//                    int[] array = new int[list.size()];
                    tempArr = line.split(",");

                    for (int i = 1; i <= tempArr.length; i++) {
                        for (int j = 1; j < tempArr.length; j++) {
                            dataSekolah.add(tempArr.get(i).get(j));
                        }
                    }
                }
                br.close();
                fitur();

            } else {
                noFile();
            }

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public static void noFile() {
        Scanner input = new Scanner(System.in);
        System.out.println("----------------");
        System.out.println("Aplikasi Pengolah Nilai Siswa");
        System.out.println("----------------");
        System.out.println("File tidak ditemukan");
        System.out.println();
        System.out.println("0. Exit");
        System.out.println("1. Kembali ke menu utama");
        int menu = input.nextInt();

        if (menu == 0) {
            System.exit(0);
        } else if (menu == 2) {
            fitur();
        } else {
            System.out.println("!! Input Error. Pilih Lagi !!");
            noFile();
        }
    }

    public void TampilModus(String filePath) {
        try {
            File file = new File("D:\\SYNERGY\\Chapter2\\Challenge2\\src\\main\\Output\\data_sekolah_Modus.txt");
            if (file.createNewFile()) {
                System.out.println("New file has been created!");
            }

            FileWriter writer = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(writer);
            bw.write(this.Modus(mod));
            bw.flush();
            bw.close();
        } catch (IOException var5) {
            var5.printStackTrace();
        }

    }

    private String Modus(Modus modus) {
        StringBuffer data = new StringBuffer("");
        data.append("Nilai kurang dari 6 || " + modus.getResult()).append("\n").append("6                   || " + modus.getElse(nilai)).append("\n").append("7                   || " + modus.getCount(nilai, 7)).append("\n").append("8                   || " + modus.getCount(nilai, 8)).append("\n").append("9                   || " + modus.getCount(nilai, 9)).append("\n").append("10                  || " + modus.getCount(nilai, 10));
        return data.toString();
    }

    private void generateModus2() {
        Modus mod = new Modus(nilai);

        try {
            File file = new File("C:/temp/direktori/data_sekolah_Modus.txt");
            if (file.createNewFile()) {
                System.out.println("New file has been created!");
            }

            FileWriter writer = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(writer);
            bw.write(this.Modus2(mod));
            bw.flush();
            bw.close();
        } catch (IOException var5) {
            var5.printStackTrace();
        }

    }

    private String Modus2(Modus modus) {
        StringBuffer data = new StringBuffer("");
        data.append("Modus = " + modus.getResult());
        return data.toString();
    }
}
